﻿namespace ExportSWC
{
    partial class ProjectOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectOptions));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBoxAsDoc = new System.Windows.Forms.CheckBox();
            this.buttonBrowseFlexOutput = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxFlexBin = new System.Windows.Forms.TextBox();
            this.cb_intrinsic_flex = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonBrowseFlashOutput = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxFlashBin = new System.Windows.Forms.TextBox();
            this.tb_comver = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_comauthor = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_uiaccess = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_desc = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_compclass = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.cb_runaem = new System.Windows.Forms.CheckBox();
            this.tb_preview = new System.Windows.Forms.TextBox();
            this.cb_createmxi = new System.Windows.Forms.CheckBox();
            this.rb_class = new System.Windows.Forms.RadioButton();
            this.rb_swf = new System.Windows.Forms.RadioButton();
            this.rb_none = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tb_icon = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_tooltip = new System.Windows.Forms.TextBox();
            this.tb_compgroup = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_compname = new System.Windows.Forms.TextBox();
            this.cb_intrinsic_cs3 = new System.Windows.Forms.CheckBox();
            this.cb_makecs3 = new System.Windows.Forms.CheckBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.openFileDialogIcon = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialogSWF = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.saveFileDialogFlex = new System.Windows.Forms.SaveFileDialog();
            this.saveFileDialogFlash = new System.Windows.Forms.SaveFileDialog();
            this.checkBoxAppendPaths = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxAppendPaths);
            this.groupBox1.Controls.Add(this.checkBoxAsDoc);
            this.groupBox1.Controls.Add(this.buttonBrowseFlexOutput);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.textBoxFlexBin);
            this.groupBox1.Controls.Add(this.cb_intrinsic_flex);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // checkBoxAsDoc
            // 
            resources.ApplyResources(this.checkBoxAsDoc, "checkBoxAsDoc");
            this.checkBoxAsDoc.Name = "checkBoxAsDoc";
            this.checkBoxAsDoc.UseVisualStyleBackColor = true;
            // 
            // buttonBrowseFlexOutput
            // 
            resources.ApplyResources(this.buttonBrowseFlexOutput, "buttonBrowseFlexOutput");
            this.buttonBrowseFlexOutput.Name = "buttonBrowseFlexOutput";
            this.buttonBrowseFlexOutput.UseVisualStyleBackColor = true;
            this.buttonBrowseFlexOutput.Click += new System.EventHandler(this.buttonBrowseFlexOutput_Click);
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // textBoxFlexBin
            // 
            this.textBoxFlexBin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            resources.ApplyResources(this.textBoxFlexBin, "textBoxFlexBin");
            this.textBoxFlexBin.Name = "textBoxFlexBin";
            this.textBoxFlexBin.Leave += new System.EventHandler(this.textBoxFlexBin_Leave);
            // 
            // cb_intrinsic_flex
            // 
            resources.ApplyResources(this.cb_intrinsic_flex, "cb_intrinsic_flex");
            this.cb_intrinsic_flex.Name = "cb_intrinsic_flex";
            this.cb_intrinsic_flex.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonBrowseFlashOutput);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.textBoxFlashBin);
            this.groupBox2.Controls.Add(this.tb_comver);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.tb_comauthor);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.tb_uiaccess);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.tb_desc);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.tb_compclass);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.cb_runaem);
            this.groupBox2.Controls.Add(this.tb_preview);
            this.groupBox2.Controls.Add(this.cb_createmxi);
            this.groupBox2.Controls.Add(this.rb_class);
            this.groupBox2.Controls.Add(this.rb_swf);
            this.groupBox2.Controls.Add(this.rb_none);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.tb_icon);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.tb_tooltip);
            this.groupBox2.Controls.Add(this.tb_compgroup);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.tb_compname);
            this.groupBox2.Controls.Add(this.cb_intrinsic_cs3);
            this.groupBox2.Controls.Add(this.cb_makecs3);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // buttonBrowseFlashOutput
            // 
            resources.ApplyResources(this.buttonBrowseFlashOutput, "buttonBrowseFlashOutput");
            this.buttonBrowseFlashOutput.Name = "buttonBrowseFlashOutput";
            this.buttonBrowseFlashOutput.UseVisualStyleBackColor = true;
            this.buttonBrowseFlashOutput.Click += new System.EventHandler(this.buttonBrowseFlashOutput_Click);
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // textBoxFlashBin
            // 
            this.textBoxFlashBin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            resources.ApplyResources(this.textBoxFlashBin, "textBoxFlashBin");
            this.textBoxFlashBin.Name = "textBoxFlashBin";
            this.textBoxFlashBin.Leave += new System.EventHandler(this.textBoxFlashBin_Leave);
            // 
            // tb_comver
            // 
            resources.ApplyResources(this.tb_comver, "tb_comver");
            this.tb_comver.Name = "tb_comver";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // tb_comauthor
            // 
            resources.ApplyResources(this.tb_comauthor, "tb_comauthor");
            this.tb_comauthor.Name = "tb_comauthor";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // tb_uiaccess
            // 
            resources.ApplyResources(this.tb_uiaccess, "tb_uiaccess");
            this.tb_uiaccess.Name = "tb_uiaccess";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // tb_desc
            // 
            resources.ApplyResources(this.tb_desc, "tb_desc");
            this.tb_desc.Name = "tb_desc";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // tb_compclass
            // 
            this.tb_compclass.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            resources.ApplyResources(this.tb_compclass, "tb_compclass");
            this.tb_compclass.Name = "tb_compclass";
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cb_runaem
            // 
            resources.ApplyResources(this.cb_runaem, "cb_runaem");
            this.cb_runaem.Name = "cb_runaem";
            this.cb_runaem.UseVisualStyleBackColor = true;
            // 
            // tb_preview
            // 
            resources.ApplyResources(this.tb_preview, "tb_preview");
            this.tb_preview.Name = "tb_preview";
            this.tb_preview.TextChanged += new System.EventHandler(this.tb_preview_TextChanged);
            this.tb_preview.DoubleClick += new System.EventHandler(this.tb_SelectAll);
            // 
            // cb_createmxi
            // 
            resources.ApplyResources(this.cb_createmxi, "cb_createmxi");
            this.cb_createmxi.Name = "cb_createmxi";
            this.cb_createmxi.UseVisualStyleBackColor = true;
            this.cb_createmxi.CheckedChanged += new System.EventHandler(this.uiSettingChanged);
            // 
            // rb_class
            // 
            resources.ApplyResources(this.rb_class, "rb_class");
            this.rb_class.Name = "rb_class";
            this.rb_class.TabStop = true;
            this.rb_class.UseVisualStyleBackColor = true;
            this.rb_class.CheckedChanged += new System.EventHandler(this.uiSettingChanged);
            // 
            // rb_swf
            // 
            resources.ApplyResources(this.rb_swf, "rb_swf");
            this.rb_swf.Name = "rb_swf";
            this.rb_swf.TabStop = true;
            this.rb_swf.UseVisualStyleBackColor = true;
            this.rb_swf.CheckedChanged += new System.EventHandler(this.uiSettingChanged);
            // 
            // rb_none
            // 
            resources.ApplyResources(this.rb_none, "rb_none");
            this.rb_none.Name = "rb_none";
            this.rb_none.TabStop = true;
            this.rb_none.UseVisualStyleBackColor = true;
            this.rb_none.CheckedChanged += new System.EventHandler(this.uiSettingChanged);
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tb_icon
            // 
            resources.ApplyResources(this.tb_icon, "tb_icon");
            this.tb_icon.Name = "tb_icon";
            this.tb_icon.TextChanged += new System.EventHandler(this.tb_icon_TextChanged);
            this.tb_icon.DoubleClick += new System.EventHandler(this.tb_SelectAll);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // tb_tooltip
            // 
            resources.ApplyResources(this.tb_tooltip, "tb_tooltip");
            this.tb_tooltip.Name = "tb_tooltip";
            this.tb_tooltip.DoubleClick += new System.EventHandler(this.tb_SelectAll);
            // 
            // tb_compgroup
            // 
            resources.ApplyResources(this.tb_compgroup, "tb_compgroup");
            this.tb_compgroup.Name = "tb_compgroup";
            this.tb_compgroup.DoubleClick += new System.EventHandler(this.tb_SelectAll);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // tb_compname
            // 
            resources.ApplyResources(this.tb_compname, "tb_compname");
            this.tb_compname.Name = "tb_compname";
            this.tb_compname.DoubleClick += new System.EventHandler(this.tb_SelectAll);
            // 
            // cb_intrinsic_cs3
            // 
            resources.ApplyResources(this.cb_intrinsic_cs3, "cb_intrinsic_cs3");
            this.cb_intrinsic_cs3.Name = "cb_intrinsic_cs3";
            this.cb_intrinsic_cs3.UseVisualStyleBackColor = true;
            // 
            // cb_makecs3
            // 
            resources.ApplyResources(this.cb_makecs3, "cb_makecs3");
            this.cb_makecs3.Name = "cb_makecs3";
            this.cb_makecs3.UseVisualStyleBackColor = true;
            this.cb_makecs3.CheckedChanged += new System.EventHandler(this.uiSettingChanged);
            // 
            // button3
            // 
            this.button3.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.button3, "button3");
            this.button3.Name = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.button4, "button4");
            this.button4.Name = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // openFileDialogIcon
            // 
            this.openFileDialogIcon.DefaultExt = "png";
            this.openFileDialogIcon.FileName = "icon.png";
            resources.ApplyResources(this.openFileDialogIcon, "openFileDialogIcon");
            this.openFileDialogIcon.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // openFileDialogSWF
            // 
            this.openFileDialogSWF.DefaultExt = "swf";
            this.openFileDialogSWF.FileName = "preview.swf";
            resources.ApplyResources(this.openFileDialogSWF, "openFileDialogSWF");
            this.openFileDialogSWF.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog2_FileOk);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            resources.ApplyResources(this.statusStrip1, "statusStrip1");
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.SizingGrip = false;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            resources.ApplyResources(this.toolStripStatusLabel1, "toolStripStatusLabel1");
            // 
            // saveFileDialogFlex
            // 
            this.saveFileDialogFlex.DefaultExt = "swc";
            resources.ApplyResources(this.saveFileDialogFlex, "saveFileDialogFlex");
            this.saveFileDialogFlex.RestoreDirectory = true;
            // 
            // saveFileDialogFlash
            // 
            this.saveFileDialogFlash.DefaultExt = "swc";
            resources.ApplyResources(this.saveFileDialogFlash, "saveFileDialogFlash");
            this.saveFileDialogFlash.RestoreDirectory = true;
            // 
            // checkBoxAppendPaths
            // 
            resources.ApplyResources(this.checkBoxAppendPaths, "checkBoxAppendPaths");
            this.checkBoxAppendPaths.Name = "checkBoxAppendPaths";
            this.checkBoxAppendPaths.UseVisualStyleBackColor = true;
            // 
            // ProjectOptions
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button4;
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProjectOptions";
            this.Load += new System.EventHandler(this.ProjectOptions_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox cb_intrinsic_flex;
        private System.Windows.Forms.CheckBox cb_intrinsic_cs3;
        private System.Windows.Forms.CheckBox cb_makecs3;
        private System.Windows.Forms.CheckBox cb_runaem;
        private System.Windows.Forms.CheckBox cb_createmxi;
        private System.Windows.Forms.TextBox tb_compgroup;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_compname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_tooltip;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tb_icon;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RadioButton rb_class;
        private System.Windows.Forms.RadioButton rb_swf;
        private System.Windows.Forms.RadioButton rb_none;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox tb_preview;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.OpenFileDialog openFileDialogIcon;
        private System.Windows.Forms.OpenFileDialog openFileDialogSWF;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_compclass;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_desc;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_uiaccess;
        private System.Windows.Forms.TextBox tb_comauthor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb_comver;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxFlexBin;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxFlashBin;
        private System.Windows.Forms.Button buttonBrowseFlexOutput;
        private System.Windows.Forms.SaveFileDialog saveFileDialogFlex;
        private System.Windows.Forms.Button buttonBrowseFlashOutput;
        private System.Windows.Forms.SaveFileDialog saveFileDialogFlash;
		private System.Windows.Forms.CheckBox checkBoxAsDoc;
        private System.Windows.Forms.CheckBox checkBoxAppendPaths;

    }
}